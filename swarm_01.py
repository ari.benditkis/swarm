"""
28.9.19
Swarm version 1. Simple following random dots.
"""
from PyQt4 import QtGui
from PyQt4.QtCore import QTimer, Qt
import numpy.random as random
import sys
import math


class Swarm(QtGui.QMainWindow):
    def __init__(self):
        # print '__init__'
        super(Swarm, self).__init__()
        self.resize(800, 600)
        self.setStyleSheet("background-color: black;")

        self.num_dots = 200
        self.dots = self.create_dots(self.num_dots)
        self.following = []
        self.create_following()
        # print 'following ' + str(self.following)
        self.md = 5  # move_distance
        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.following_timer = QTimer()
        self.following_timer.timeout.connect(self.create_following)

    def run_swarm(self, interval, switch, stop):
        # print 'run_swarm'
        self.timer.start(1000*interval)
        QTimer.singleShot(1000*stop, self.timer.stop)
        if switch != 0:
            self.following_timer.start(1000*switch)
            QTimer.singleShot(1000*stop, self.following_timer.stop)

    def create_dots(self, num):
        # print 'create_dots'
        dots = list(zip(random.randint(self.width(), size=num), random.randint(self.height(), size=num)))
        # print dots
        return dots

    def create_following(self):
        print 'create_following'
        new_following = []
        for i in xrange(self.num_dots):
            follow = random.randint(self.num_dots)
            while follow == i:
                follow = random.randint(self.num_dots)
            new_following += [follow]
        self.following = new_following

    def paintEvent(self, event):
        # print 'paintEvent'
        painter = QtGui.QPainter(self)
        pen = QtGui.QPen()
        pen.setWidth(4)
        pen.setCapStyle(20)  # Qt.RoundCap
        pen.setColor(Qt.white)
        painter.setPen(pen)

        self.update_dots()
        self.paint_dots(painter)

    def update_dots(self):
        # print 'update_dots'
        # self.dots = self.create_dots(self.num_dots)
        new_locations = []
        for i in xrange(self.num_dots):
            dot = self.dots[i]
            follow_dot = self.dots[self.following[i]]
            # print 'dot ' + str(dot)
            # print 'follow_dot ' + str(follow_dot)
            if dot != follow_dot:
                dx, dy = float(dot[0]), float(dot[1])  # dot_x, dot_y
                fx, fy = float(follow_dot[0]), float(follow_dot[1])  # follow_dot_x, follow_dot_y
                bfv = [fx - dx, fy - dy]  # dot_follow_vector
                norm = math.sqrt(bfv[0]**2 + bfv[1]**2)
                # print 'norm ' + str(norm)
                mv = [(bfv[0]/norm)*self.md, (bfv[1]/norm)*self.md]  # move_vector
                new_dot = (dot[0] + mv[0], dot[1] + mv[1])
                # print 'new_dot ' + str(new_dot)
                new_locations += [new_dot]
            else:
                new_locations += [dot]
        # print 'new_locations ' + str(new_locations)
        self.dots = new_locations

    def paint_dots(self, painter):
        # print 'paint_dots'
        for dot in self.dots:
            painter.drawPoint(*dot)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = Swarm()
    window.show()
    window.run_swarm(1.0/24.0, 0, 20)
    sys.exit(app.exec_())
