"""
Very bad gravity point swarm. A total failure.
"""
from PyQt4 import QtGui
from PyQt4.QtCore import QTimer, Qt
from numpy import random, array
import math
import sys


class GravityPoint(object):
    def __init__(self, mass, location):
        self.mass = mass
        self.location = location
        self.dots = []

    def add_dot(self, dot):
        self.dots += [dot]


class Dot(object):
    def __init__(self, mass, location, velocity, max_speed, gravity_point, min_force):
        self.mass = mass
        self.location = location
        self.velocity = velocity
        self.max_speed = max_speed
        self.gravity_point = gravity_point
        self.min_force = min_force

    def update(self, time):
        force = self.calculate_force(self.mass, self.gravity_point.mass, self.location, self.gravity_point.location)
        self.location = self.location + (self.velocity * time) + ((force * (time ** 2)) / (2 * self.mass))
        new_velocity = self.velocity + ((force * time) / self.mass)
        if magnitude(new_velocity) > self.max_speed:
            self.velocity = unit(new_velocity)*self.max_speed

    def calculate_force(self, dot_mass, gp_mass, dot_location, gp_location):
        radius = magnitude(gp_location - dot_location)
        # if radius < 1:
        #     return array([0, 0])
        force_magnitude = (G*dot_mass*gp_mass)/(radius**2)
        if force_magnitude < self.min_force:
            force_magnitude = self.min_force
        elif force_magnitude > maximum_force:
            force_magnitude = maximum_force
        print 'force_magnitude' + str(force_magnitude)
        force = unit(gp_location - dot_location)*force_magnitude
        return force


class Swarm(object):
    def __init__(self, swarm_size, num_gp, window_width, window_height, dots_mass_range, gp_mass_range,
                 start_speed_range, max_speed_range, min_force_range):
        self.swarm_size = swarm_size
        self.num_gp = num_gp
        self.window_width = window_width
        self.window_height = window_height
        self.dots_mass_range = dots_mass_range
        self.gp_mass_range = gp_mass_range
        self.start_speed_range = start_speed_range
        self.max_speed_range = max_speed_range
        self.min_force_range = min_force_range
        self.gravity_points = self.create_gravity_points()
        self.dots = self.create_dots()

    def create_gravity_points(self):
        gp = []
        for i in xrange(self.num_gp):
            mass = random.randint(*self.gp_mass_range)
            location = array([random.randint(150, self.window_width - 150),
                              random.randint(150, self.window_height - 150)])
            gp += [GravityPoint(mass, location)]
        return gp

    def create_dots(self):
        dots = []
        for i in xrange(self.swarm_size):
            mass = random.randint(*self.dots_mass_range)
            location = array([random.randint(self.window_width), random.randint(self.window_height)])
            velocity = unit(2 * random.random_sample((2,)) - 1)*random.randint(*self.start_speed_range)
            gravity_point = self.gravity_points[random.randint(self.num_gp)]
            min_force = random.randint(*self.min_force_range)
            new_dot = Dot(mass, location, velocity, random.randint(*self.max_speed_range), gravity_point, min_force)
            gravity_point.add_dot(new_dot)
            dots += [new_dot]
        return dots

    def update_dots(self, time):
        for dot in self.dots:
            dot.update(time)

    def update_gravity_point(self, index, update_range):
        old_gp_dots = self.gravity_points[index].dots
        new_mass = random.randint(*self.gp_mass_range)
        new_location = array([random.randint(self.window_width), random.randint(self.window_height)])
        new_gp = GravityPoint(new_mass, new_location)
        self.gravity_points[index] = new_gp
        for dot in old_gp_dots:
            gp = self.gravity_points[random.randint(self.num_gp)]
            dot.gravity_point = gp
            gp.add_dot(dot)
        QTimer.singleShot(1000 * random.randint(*update_range), lambda: self.update_gravity_point(index, update_range))


class Window(QtGui.QMainWindow):
    def __init__(self, swarm_size, num_gp, window_width, window_height, dots_mass_range, gp_mass_range,
                 start_speed_range, max_speed_range, update_interval, gp_update_range, min_force_range):
        super(Window, self).__init__()
        if window_width and window_height:
            self.resize(window_width, window_height)
        else:
            self.showMaximized()
        self.setStyleSheet("background-color: black;")
        self.swarm = Swarm(swarm_size, num_gp, window_width, window_height, dots_mass_range, gp_mass_range,
                           start_speed_range, max_speed_range, min_force_range)
        self.update_interval = update_interval
        self.gp_update_range = gp_update_range
        self.update_timer = QTimer()
        self.update_timer.timeout.connect(self.update)

    def start_swarm(self):
        self.update_timer.start(1000 * self.update_interval)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        pen = QtGui.QPen()

        pen.setWidth(7)
        pen.setCapStyle(Qt.RoundCap)
        pen.setColor(Qt.red)
        painter.setPen(pen)

        for gp in self.swarm.gravity_points:
            painter.drawPoint(*gp.location)

        pen.setWidth(5)
        pen.setCapStyle(Qt.RoundCap)
        pen.setColor(Qt.white)
        painter.setPen(pen)

        self.swarm.update_dots(self.update_interval)
        for dot in self.swarm.dots:
            painter.drawPoint(*dot.location)


def magnitude(vector):
    return math.sqrt(vector[0] ** 2 + vector[1] ** 2)


def unit(vector):
    return vector/magnitude(vector)


G = 1000000

my_swarm_size = 10
my_num_gp = 1
my_window_width = 600
my_window_height = 600
my_dots_mass_range = (100, 150)
my_gp_mass_range = (100, 150)
my_start_speed_range = (100, 101)
my_max_speed_range = (10000, 15000)
my_update_interval = 1.0/24.0
my_gp_update_range = (5, 10)
my_min_force_range = (100000, 100001)
maximum_force = 1000000


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = Window(my_swarm_size, my_num_gp, my_window_width, my_window_height, my_dots_mass_range, my_gp_mass_range,
                    my_start_speed_range, my_max_speed_range, my_update_interval, my_gp_update_range,
                    my_min_force_range)
    window.show()
    window.start_swarm()
    sys.exit(app.exec_())
